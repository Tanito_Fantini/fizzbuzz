﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FizzBuzzCSharpTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestNumber()
        {
            int number = 1;
            string expected = FizzBuzzCSharp.Fizzbuzz.run(number);

            Assert.AreEqual(expected, number.ToString());
        }

        [TestMethod]
        public void TestFizz()
        {
            int number = 3;
            string expected = FizzBuzzCSharp.Fizzbuzz.run(number);

            Assert.AreEqual(expected, "fizz");
        }

        [TestMethod]
        public void TestBuzz()
        {
            int number = 5;
            string expected = FizzBuzzCSharp.Fizzbuzz.run(number);

            Assert.AreEqual(expected, "buzz");
        }

        [TestMethod]
        public void TestFizzBuzz()
        {
            int number = 15;
            string expected = FizzBuzzCSharp.Fizzbuzz.run(number);

            Assert.AreEqual(expected, "fizzbuzz");
        }
    }
}
