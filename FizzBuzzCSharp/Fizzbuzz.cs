﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzzCSharp
{
    public class Fizzbuzz
    {
        private static bool divisible(int divisor, int number)
        {
            return number % divisor == 0;
        }

        public static string run(int number)
        {
            string salida = number.ToString();

            if (number == 0)
            {
                return salida;
            }

            if (divisible(3, number) && divisible(5, number))
            {
                return "fizzbuzz";
            }

            if (divisible(3, number))
            {
                return "fizz";
            }

            if (divisible(5, number))
            {
                return "buzz";
            }

            return salida;
        }

        public static void print(int number)
        {
            for (int i=0; i<=number; i++)
            {
                Console.WriteLine("{0}: {1}", i, run(i));
            }
        }
    }
}
