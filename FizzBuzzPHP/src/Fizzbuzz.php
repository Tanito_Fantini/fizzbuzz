<?php

declare( strict_types = 1 );

final class Fizzbuzz {

  public static function divisible(int $divisor, int $number) : bool {
    return $number % $divisor === 0;
  }

  public static function run(int $number) {
    if (!is_int($number)) {
      throw new InvalidArgumentException('The argument must be an integer');
    }
    
    if ($number === 0) {
      return 0;
    }

    if (Fizzbuzz::divisible(3, $number) && Fizzbuzz::divisible(5, $number)) {
      return 'fizzbuzz';
    }

    if (Fizzbuzz::divisible(3, $number)) {
      return 'fizz';
    }

    if (Fizzbuzz::divisible(5, $number)) {
      return 'buzz';
    }

    return $number;
  }

  public static function printResult(int $number) : void {
    for ($i=0; $i<=$number; $i++) {
      echo $i . ': ' . Fizzbuzz::run($i) . PHP_EOL;
    }
  }

}

Fizzbuzz::printResult(16);