<?php

declare( strict_types = 1 );

use PHPUnit\Framework\TestCase;

final class FizzbuzzTest extends TestCase {

  public function setUp() : void {
  }

  public function testReturn1() : void {
    $this->assertEquals(1, Fizzbuzz::run(1));
  }

  public function testReturnFizz() : void {
    $this->assertEquals('fizz', Fizzbuzz::run(3));
  }

  public function testReturnBuzz() : void {
    $this->assertEquals('buzz', Fizzbuzz::run(5));
  }

  public function testReturnFizzbuzz() : void {
    $this->assertEquals('fizzbuzz', Fizzbuzz::run(15));
  }

  public function testNullValue() : void {
    $this->expectException(InvalidArgumentException::class);
    Fizzbuzz::run(null);
  }

}