const fizzbuzz = require('./fizzbuzz');
const randomNumber = () => Math.floor((Math.random() * 100) + 1) + 1;

describe('fizzbuzz', () => {
  test('test', () => {
    expect(true).toBe(true);
  });

  test('should print error message if it receives not a number', () => {
    const expected = 'Error. The argument must be a number',
          result   = fizzbuzz(null);

    expect(expected).toBe(result);
  });

  test('should print 1 if it receives 1', () => {
    const expected = 1,
          result   = fizzbuzz(1);

    expect(expected).toBe(result);
  });

  test('should print fizz if it receives a multiple of 3', () => {
    const expected = 'fizz',
          result   = fizzbuzz(3);

    expect(expected).toBe(result);
  });

  test('should print buzz if it receives a multiple of 5', () => {
    const expected = 'buzz',
          result   = fizzbuzz(5);

    expect(expected).toBe(result);
  });

  test('should print fizzbuzz if it receives a multiple of 3 and 5', () => {
    const expected = 'fizzbuzz',
          result   = fizzbuzz(15);

    expect(expected).toBe(result);
  });
});