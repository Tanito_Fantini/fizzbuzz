const divisible = (divisor, number) => number % divisor === 0;

function fizzBuzz(number) {
  if (typeof number !== 'number') {
    return 'Error. The argument must be a number';
  }

  if (number === 0) {
    return 0;
  }

  if (divisible(3, number) && divisible(5, number)) {
    return 'fizzbuzz';
  }

  if (divisible(3, number)) {
    return 'fizz';
  }

  if (divisible(5, number)) {
    return 'buzz';
  }

  return number;
}

function print(number) {
  for (let i=0; i<=number; i++) {
    console.log(`${i}: ${fizzBuzz(i)}`);
  }
}

// print(16);

module.exports = fizzBuzz;